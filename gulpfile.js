var gulp = require('gulp');
var plumber = require('gulp-plumber');
var rename = require('gulp-rename');
var sass = require('gulp-sass');
var csslint = require('gulp-csslint');
var autoPrefixer = require('gulp-autoprefixer');
var cssComb = require('gulp-csscomb');
var cmq = require('gulp-merge-media-queries');
var cssnano = require('gulp-cssnano');
var sourcemaps = require('gulp-sourcemaps');
var babel = require('gulp-babel');
var jshint = require('gulp-jshint');
var browserify = require('gulp-browserify');
var source = require('vinyl-source-stream');
var express = require('express');
var browserSync = require('browser-sync');
var gutil = require('gulp-util');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var jade = require('gulp-jade');
var prettify = require('gulp-prettify');
var mainBowerFiles = require('gulp-main-bower-files');
var gulpFilter = require('gulp-filter');
var marked = require('jstransformer')(require('jstransformer-marked'));


var server; 

gulp.task('sass', function () {
    gulp.src(['src/styles/wrkflo.scss'])
        .pipe(plumber({
            handleError: function (err) {
                console.log(err);
                this.emit('end');
            }
        }))
        .pipe(sass())
        .pipe(autoPrefixer())
        .pipe(cssComb())
        .pipe(cmq({
            log: false
        }))
        .pipe(concat('wrkflo.css'))
        .pipe(gulp.dest('dist/assets/css'))
        .pipe(gulp.dest('mockups/assets/css'))
        .pipe(reload())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(cssnano())
        .pipe(gulp.dest('dist/assets/css'))
        .pipe(gulp.dest('mockups/assets/css'))
        .pipe(reload())
    gulp.src(['src/styles/docs/**/*.scss'])
        .pipe(plumber({
            handleError: function (err) {
                console.log(err);
                this.emit('end');
            }
        }))
        .pipe(sass())
        .pipe(autoPrefixer())
        .pipe(cssComb())
        .pipe(cmq({
            log: false
        }))
        .pipe(concat('docs.css'))
        .pipe(gulp.dest('docs/assets/css'))
        .pipe(reload())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(cssnano())
        .pipe(gulp.dest('docs/assets/css'))
        .pipe(reload())
});

gulp.task('babel', function () {
    gulp.src(['src/scripts/**/*.js'])
        .pipe(plumber({
            handleError: function (err) {
                console.log(err);
                this.emit('end');
            }
        }))
        .pipe(babel())
        .pipe(concat('wrkflo.js'))
        .pipe(jshint())
        .pipe(jshint.reporter('default'))
        .pipe(browserify())
        .pipe(gulp.dest('dist/assets/js'))
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(uglify())
        .pipe(gulp.dest('dist/assets/js'))
        .pipe(reload());
});

gulp.task('jade', function () {
    gulp.src(['src/html/docs/*.jade'])
        .pipe(plumber({
            handleError: function (err) {
                console.log(err);
                this.emit('end');
            }
        }))
        .pipe(jade())
        .pipe(prettify({
            indent_size: 4,
            indent_inner_html: true,
            unformatted: ['pre', 'code']
        }))
        .pipe(gulp.dest('docs/'))
        .pipe(reload());
    gulp.src(['src/html/mockups/*.jade'])
        .pipe(plumber({
            handleError: function (err) {
                console.log(err);
                this.emit('end');
            }
        }))
        .pipe(jade())
        .pipe(prettify({
            indent_size: 4,
            indent_inner_html: true,
            unformatted: ['pre', 'code']
        }))
        .pipe(gulp.dest('mockups/'))
        .pipe(reload());
});

gulp.task('server', function () {
    server = express();
    server.use(express.static('docs'));
    server.listen(8000);
    browserSync({
        proxy: 'localhost:8000'
    });
});

gulp.task('build', ['babel', 'sass', 'jade']);

gulp.task('watch', function () {
    gulp.watch('src/scripts/**/*.js', ['babel']);
    gulp.watch('src/styles/**/*.scss', ['sass']);
    gulp.watch('src/html/**/*.jade', ['jade']);
});

gulp.task('default', ['build', 'watch', 'server']);


function reload() {
    if (server) {
        return browserSync.reload({
            stream: true
        });
    }
    return gutil.noop();
}